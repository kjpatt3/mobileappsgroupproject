import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import navigationService from './services/NavigationService';

export default class App extends Component{
  render() {
    return (
        navigationService.getTopNavigator()
    );
  }
}
