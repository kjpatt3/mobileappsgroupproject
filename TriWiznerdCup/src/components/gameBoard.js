import React, { Component } from 'react';
import { View, TouchableWithoutFeedback, Text } from 'react-native';
import GameCard from './gameCard.js';
import styles from '../styles/Style';

// The generalized actual game class
export default class GameBoard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            imageHasBeenShown: [],
            correctMatches: [],
            centerColorIndex: 0,
            imageIndices: [],
            showFace: [],
            indicateCorrect: [],
            indicateIncorrect: []
        };

        this.images = [
            require('../resources/chevrons.jpg'),
            require('../resources/floral.jpg'),
            require('../resources/giraffe.jpg'),
            require('../resources/hex.jpg'),
            require('../resources/maze.jpg'),
            require('../resources/polygons.jpg'),
            require('../resources/roses.jpg'),
            require('../resources/smoke.jpg'),
            require('../resources/triangles.jpg'),
            require('../resources/weave.jpg'),
        ]
    }

    
    componentWillReceiveProps(nextProps){
        if (!this.props.start && nextProps.start) {
            this.beginGame();
        }
    }

    componentWillMount(){
        this.initializeCards();   
    }

    // If the component is unmounting stop the timer
    componentWillUnmount() {
        clearInterval(this._timer);
    }


    render() {
        // The container to hold the columns
        let board = [];
        let columnA  = [];
        let columnB  = [];
        let columnC  = [];

        columnA.push( <GameCard
            onFlip = {() => this.buttonPressed(0)}
            key={1}
            flipped = {this.state.showFace[0]}
            imageURL = {this.images[this.state.imageIndices[0]]}
            isCorrect = {this.state.indicateCorrect[0]}
            isIncorrect = {this.state.indicateIncorrect[0]}
            isHidden = {false}
            size = {100}
        />);
        columnA.push( <GameCard
            onFlip = {() => this.buttonPressed(1)}
            key={2}
            flipped = {this.state.showFace[1]}
            imageURL = {this.images[this.state.imageIndices[1]]}
            isCorrect = {this.state.indicateCorrect[1]}
            isIncorrect = {this.state.indicateIncorrect[1]}
            isHidden = {false}
            size = {100}
        />);
        columnA.push( <GameCard
            onFlip = {() => this.buttonPressed(2)}
            key={3}
            flipped = {this.state.showFace[2]}
            imageURL = {this.images[this.state.imageIndices[2]]}
            isCorrect = {this.state.indicateCorrect[2]}
            isIncorrect = {this.state.indicateIncorrect[2]}
            isHidden = {false}
            size = {100}
        />);
        
        columnB.push( <GameCard
            onFlip = {() => this.buttonPressed(3)}
            key={4}
            flipped = {this.state.showFace[3]}
            imageURL = {this.images[this.state.imageIndices[3]]}
            isCorrect = {this.state.indicateCorrect[3]}
            isIncorrect = {this.state.indicateIncorrect[3]}
            isHidden = {false}
            size = {100}
        />);
        columnB.push( <GameCard
            onFlip = {() => this.buttonPressed(-1)}
            key={5}
            flipped = {true}
            imageURL = {this.images[this.state.imageIndices[this.state.centerColorIndex]]}
            isHidden = {!this.state.visible}
            size = {100}            
        />);
        columnB.push( <GameCard
            onFlip = {() => this.buttonPressed(4)}            
            key={6}
            flipped = {this.state.showFace[4]}
            imageURL = {this.images[this.state.imageIndices[4]]}
            isCorrect = {this.state.indicateCorrect[4]}
            isIncorrect = {this.state.indicateIncorrect[4]}
            isHidden = {false}
            size = {100}
        />);

        columnC.push( <GameCard
            onFlip = {() => this.buttonPressed(5)}
            key={7}
            flipped = {this.state.showFace[5]}
            imageURL = {this.images[this.state.imageIndices[5]]}
            isCorrect = {this.state.indicateCorrect[5]}
            isIncorrect = {this.state.indicateIncorrect[5]}
            isHidden = {false}
            size = {100}
        />);    
        columnC.push( <GameCard
            onFlip = {() => this.buttonPressed(6)}
            key={8}
            flipped = {this.state.showFace[6]}
            imageURL = {this.images[this.state.imageIndices[6]]}
            isCorrect = {this.state.indicateCorrect[6]}
            isIncorrect = {this.state.indicateIncorrect[6]}
            isHidden = {false}
            size = {100}
        />); 
        columnC.push( <GameCard
            onFlip = {() => this.buttonPressed(7)}
            key={9}
            flipped = {this.state.showFace[7]}
            imageURL = {this.images[this.state.imageIndices[7]]}
            isCorrect = {this.state.indicateCorrect[7]}
            isIncorrect = {this.state.indicateIncorrect[7]}
            isHidden = {false}
            size = {100}
        />);

        // Add each column the row container
        board.push(<View style={styles.columnContainer} key={1}> 
                {columnA} 
            </View>
        );
        board.push(<View style={styles.columnContainer} key={2}> 
                {columnB} 
            </View>
        );
        board.push(<View style={styles.columnContainer} key={3}> 
                {columnC} 
            </View>
        );
    
        return (
            <TouchableWithoutFeedback>
                <View style={styles.container}>
                <View style={styles.gameBoard}>{board}</View>
                </View>
            </TouchableWithoutFeedback>
        );
    }

    initializeCards() {
        var indexAvailable = [];
        var initiallyFalse = [];
        var indices = [];

        for(var i = 0; i < 8; ++i){
            indexAvailable.push(true);
            initiallyFalse.push(false);
        }
        for(var i = 0; i < 8; ++i){
            var rand;
            do{
                rand = Math.floor(Math.random() * 8);
            }
            while(!indexAvailable[rand]);
            indices.push(rand);
            indexAvailable[rand] = false;
        }
        this.setState((prevState, prop) => {
            return {
                imageIndices : indices,
                imageHasBeenShown: initiallyFalse,
                showFace: initiallyFalse,
                correctMatches: initiallyFalse,
                indicateIncorrect: initiallyFalse,
                indicateCorrect: initiallyFalse
            }
        });

    }

    showCardFace(showIndex) {
        this.setState((prevState, prop) => {
            let sF = [];
            for(var i = 0; i < 8; ++i){
                sF[i] = false;
            }
            if(showIndex != 8) sF[showIndex] = true;
            return {
                showFace: sF
            }
        });
        if(showIndex == 8){
            clearInterval(this._timer);
            this.setState(() => {
                return {
                    visible: true
                }
            });
            this.testMemory();
        }

    }

    testMemory(){
        var center;
        do{
            center = Math.floor(Math.random() * 8);
        } while(this.state.imageHasBeenShown[center])
        this.setState((prevState) => {
            var images = [...prevState.imageHasBeenShown];
            images[center] = true;
            return {
                imageHasBeenShown: images,
                centerColorIndex: center
            }
        })

    }

    beginGame(){
        var i = 0;
        this._timer = setInterval(() => {this.showCardFace(i++);}, 1000);
    }

    score() {
        var numCorrect = 0;
        for(var i = 0; i < 8; ++i){
            if(this.state.correctMatches[i]) numCorrect++;
        }
        if(numCorrect==8)
            setTimeout(() => {
                var incorrect = [];
                for(var i = 0; i < 8; ++i){
                    incorrect[i] = !this.state.correctMatches[i];
                }
                this.setState(() => {
                    return {
                        indicateIncorrect: incorrect
                    }
                })
                setTimeout(() => {
                    this.setState((prevState) => {
                        return {
                            indicateCorrect: prevState.correctMatches
                        }
                    })
                }, 3000);
            }, 1000);
            else {
                setTimeout(() => {
                    this.setState((prevState) => {
                        return {
                            indicateCorrect: prevState.correctMatches
                        }
                    })
                }, 3000);
            }
            this.props.score(numCorrect);
            this.props.stopTimer();
    }

    buttonPressed(i){
        if(i == -1) return;
        var correct = false;
        var finished = true;
        var center = 0;

        if(this.state.imageIndices[i] == this.state.centerColorIndex) correct = true;

        for(var j = 0; j < 8; ++j){
            if(!this.state.imageHasBeenShown[j]) finished = false;
        }

        if(!finished){
            do{
                center = Math.floor(Math.random() * 8);
            } while(this.state.imageHasBeenShown[center]);
        }
        
        
        
        this.setState((prevState) => {
            var images = [...prevState.imageHasBeenShown];
            images[center] = true;
            var correctMatch = [...prevState.correctMatches];
            if(correct) {
                correctMatch[i] = true;
            }
            return {
                imageHasBeenShown: images,
                centerColorIndex: center,
                correctMatches: correctMatch
            }
        })

        if(finished) {
            this.setState(() => {
                return {
                    visible: false
                }
            });
            this.score();
        }
    }
}
