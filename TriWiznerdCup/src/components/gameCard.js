import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {AppRegistry, StyleSheet, Image, View} from 'react-native';
import {Container, Header, Content, Card, CardItem, Body, Text} from 'native-base';

import styles from '../styles/Style';

export default class GameCard extends Component {

  static propTypes = {
    onFlip: PropTypes.func,
    flipped: PropTypes.bool,
    imageURL: PropTypes.string,
    isHidden: PropTypes.bool,
    isCorrect: PropTypes.bool,
    isIncorrect: PropTypes.bool,
    size: PropTypes.number,
  };

  render() {
    let propStyles = {width: this.props.size, height: this.props.size, display: this.props.isHidden ? 'none' : null}
    let sheetStyles = styles.cardImage
    let graphicStyle = [propStyles, styles.graphicOverlay]
    let style = [propStyles, sheetStyles]
    let cardJsx;
    let imageURL = this.props.imageURL
    if (this.props.isHidden === true) {
        cardJsx = (
          <View>
            <Image source={require('../resources/nobackground.png')} style={style}/>
          </View>
        )
    } else if (this.props.flipped === true) {
      if(this.props.isCorrect === true) {
        cardJsx = (
          <View>
            <Image source={imageURL} style={style}/>
            <Image source={require('../resources/checkmark.png')} style={graphicStyle}/>
          </View>
        )
      } else if (this.props.isIncorrect === true) {
        cardJsx = (
          <View>
            <Image source={imageURL} style={style}/>
            <Image source={require('../resources/redcross.png')} style={graphicStyle}/>
          </View>
        )
      } else {
        cardJsx = (
           <Image source={imageURL} style={style}/>
        )
      }
    } else {
      cardJsx = (
        <Image source={require('../resources/cardback.jpg')} style={style}/>
      )
    }
    return (
        <Card>
          <CardItem cardBody button onPress={() => this.props.onFlip()}>
            {cardJsx}
          </CardItem>
        </Card>
    )
  }
}
