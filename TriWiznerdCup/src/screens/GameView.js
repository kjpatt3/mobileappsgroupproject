import React, {Component} from 'react';
import { Container, Content, Button, Icon, SwipeRow, Text, } from 'native-base';
import {Platform, StyleSheet, View,TouchableOpacity} from 'react-native';
import styles from '../styles/Style'
import GameBoard from '../components/gameBoard'

export default class GameView extends Component {
  static navigationOptions = {
    headerStyle: styles.homeHeader,
    title: 'GameView'
  }
    constructor(props) {
        super(props)
        this.state = { 
            time: 0,
            start: false,
            score:0 
            }

           
      }
    
    render() {
      return (
        <Container style={styles.container}>
          <Content>
            {/* <View style = {styles.containerGame}> */}
            <View>
            <Text></Text>
            <Text></Text>
                <View  style= {styles.header}>
                    <Text style = {styles.text}>Score: {this.state.score}</Text>
                    <Text>      </Text>
                    <Text style = {styles.text}>Timer: {this.state.time}</Text>
                </View>
                <GameBoard
                   start = {this.state.start}
                   stopTimer = {() => this.stopTimer()}
                   score = {(val) => this.incrementScore(val)}
                />
                <TouchableOpacity style ={styles.startGame}
                onPress = {this.startTimer}>
                    <Text style ={styles.text}>Start Game</Text>
                </TouchableOpacity>
                <Text></Text>
                {/* <TouchableOpacity style ={styles.stopGame}
                onPress = {this.stopTimer}>
                    <Text style ={styles.text}>Stop Game</Text>
                </TouchableOpacity> */}
            </View>
          </Content>
        </Container>
      );
    }

    startTimer = ()=> {
        this.interval = setInterval(() => {
            this.setState(prevState => ({
              time: prevState.time + 1,
              start: true
            }));
          }, 1000);
    }
    stopTimer = ()=>{
        clearInterval(this.interval);
        this.setState(prevState => ({
            time: 0
          }));
    }
    incrementScore = (val) => {
        this.setState(prevState=>({
            score: prevState.score + val
        }))
    }
  }
