import React, { Component } from 'react';
import {
    TouchableOpacity,
    View,
    FlatList,
    TextInput,
    // Text,
} from 'react-native';
import { Container, Content, Button, Icon, SwipeRow, Text, } from 'native-base';

import navigationService from '../services/NavigationService';
import GameCard from '../components/gameCard';
import styles from '../styles/Style';

export default class HomeScreen extends Component {

    static navigationOptions = {
        headerStyle: styles.homeHeader,
        title: 'Home'

    }
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Container style={styles.container}>
                <Content >
                <View >
                    <Text style={styles.title}>Matching Game</Text>
                    <Text style={styles.description}>Match the middle card with one of the surrounding cards until all surrounding cards have a match.</Text>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => { navigationService.navigate('Game') }}
                    >
                        <Text>Start Game</Text>
                    </TouchableOpacity>
                </View>
                <GameCard/>
                </Content>
            </Container>
            
        );
    } 
}
