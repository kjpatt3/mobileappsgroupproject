import { StyleSheet, Dimensions  } from 'react-native';

export default StyleSheet.create({
    cardImage: {
      display: 'flex',
    },
    homeHeader: { backgroundColor: '#ffd8de' },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        
        backgroundColor: '#fff2f4',
    },
    info: {
        alignItems: 'center',
        textAlign: 'center',
    },
    title: {
        fontSize: 40,
        textAlign: 'center',
        marginTop: 30,
    },
    description: {
        textAlign: 'center',
        padding: 30
    },
    button: {
        backgroundColor: 'grey',
        alignSelf: 'center',
        margin: 10,
        padding: 10,
        borderRadius: 20,
        width: 150,
        alignItems: 'center',
    },
    containerGameBoard: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#f2f2f2',
    },
    // The style for the actual gameplay area
    gameBoard : {
      flex: 1,
      flexDirection: 'row',
      width: Dimensions.get('window').width,
      justifyContent: 'space-evenly',
      alignItems: 'center',
      backgroundColor: '#f2f2f2',
    }, 
    // The style for the gameplay column view
    columnContainer : {
        height: Dimensions.get('window').height - 300,
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        backgroundColor: '#f2f2f2',
    }, 
    containerGame: {
      flex: 1,
      flexDirection:'column',
      justifyContent: 'center',
      backgroundColor: '#F5FCFF',
      alignItems:'center'
    },
    header:{
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor: '#F5FCFF',
      justifyContent: 'space-between'
    },
    startGame: {
      backgroundColor: '#00FF00',
      fontSize: 40,
      width: window.width/2,
      padding: 20,
      fontSize: 60,
      justifyContent:'center',

  },
  stopGame:{
    backgroundColor: '#FF0000',
    fontSize: 40,
    width: window.width/2,
    padding: 20,
    fontSize: 60,
    justifyContent:'center',
  },
  text: {
    fontWeight: 'bold',
    fontSize: 30,
  }
});
